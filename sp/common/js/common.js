$(function(){

	//メニュー開き
	$('.btn_menu').click(function() {
		$('nav').fadeIn(300, 'jswing');
	});

	//メニュー閉じ
	$('#close').click(function() {
		$('nav').fadeOut(300, 'jswing');
	});

});